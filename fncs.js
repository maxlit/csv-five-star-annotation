// (A) FILE READER + HTML ELEMENTS
let reader = new FileReader(),
    picker = document.getElementById('csvPick'),
    table = document.getElementById('csvTable'),
    hasRating= false;

// (B) READ CSV ON FILE PICK
picker.onchange = () => reader.readAsText(picker.files[0]);

// (C) READ CSV & GENERATE TABLE
reader.onloadend = () => {
  var header=true;
  table.innerHTML = '';
  var sep = $('#sep').val();
  sep = sep.replace(/\\\\/g, '%5C'); // replace double backslashes with "%5C"
  sep = decodeURIComponent(sep); // decode the percent-encoded string
  const CSVdata= CSV.parse(reader.result, null, sep = sep)
  hasRating= CSVdata[0][0]==="rating" ? true : false //checks if the file has "rating" column
  
  for (let row of CSVdata) {
    let tr = table.insertRow();
    if (header) {
      tr.setAttribute('style', 'background-color:beige')
    }
    let td = tr.insertCell();
    if (header) {
      header=false;
      td.innerHTML  = "<i value='Rating'>Rating</>"
      td.setAttribute("value", "rating")
    } else {
      td.innerHTML = "<div class='stars'>" + 
      "<i class='fa fa-star-o' id='star1' data-rating='1' value=1></i>" +
      "<i class='fa fa-star-o' id='star2' data-rating='2' value=2></i>" +
      "<i class='fa fa-star-o' id='star3' data-rating='3' value=3></i>" +
      "<i class='fa fa-star-o' id='star4' data-rating='4' value=4></i>" +
      "<i class='fa fa-star-o' id='star5' data-rating='5' value=5></i>" +
      "</div>";
      td.setAttribute("value", 0)
    }
    td.classList.add("output");
    if (hasRating) {
      value=row.shift()
      td.setAttribute("value", value)
    }
    for (let col of row) {
      let td = tr.insertCell();
      td.innerHTML = col;
      td.setAttribute('value', col);
    }
  }
  initRating(".stars", "i", ".output", hasRating);
};

// render 5 star ratings - not mine, taken from somewhere, slightly re-packaged
function initRating(wrapperSelector, starsSelector, outputSelector, hasRating) {
  const wrapper = document.querySelectorAll(wrapperSelector);
  wrapper.forEach(function(currentWrapper) {
      const stars = currentWrapper.querySelectorAll(starsSelector);
      if (hasRating) { //renders stars if file has "rating" column
        stars.forEach(function(star,i) {
          const initValue= Number(star.closest(outputSelector).getAttribute("value"))
          if (i+1 <= initValue) {
            star.classList.add("active", "fa-star");
            star.classList.remove("fa-star-o");
          }
        })
        generateCountGroupByTable();
      }
      stars.forEach(function(star) {
        star.addEventListener("click", function(e) {
          const rating = this.dataset["rating"];
          outputs = this.closest(outputSelector);
          console.log(rating);
          let nextSibling = this.nextElementSibling;
          let previousSibling = this.previousElementSibling;
          this.classList.add("active", "fa-star");
          this.classList.remove("fa-star-o");
          while (nextSibling !== null && (nextSibling.classList.contains("fa-star") || nextSibling.classList.contains("fa-star-o"))) {
            nextSibling.classList.remove("active", "fa-star");
            nextSibling.classList.add("fa-star-o");
            nextSibling = nextSibling.nextElementSibling;
          }
          while (previousSibling !== null && (previousSibling.classList.contains("fa-star") || previousSibling.classList.contains("fa-star-o"))) {
            previousSibling.classList.add("active", "fa-star");
            previousSibling.classList.remove("fa-star-o");
            previousSibling = previousSibling.previousElementSibling;
          }
        if (outputs != null) {
              outputs.setAttribute("value", rating);
          } else {
            console.log("==0");
          }
          generateCountGroupByTable();
        });
      });
});
}



// EXPORT CSV
// https://stackoverflow.com/questions/7161113/how-do-i-export-html-table-data-as-csv-file
function exportTableToCSV($table, filename, colDelim) {

  var $rows = $table.find('tr:has(td)'),

  // Temporary delimiter characters unlikely to be typed by keyboard
  // This is to avoid accidentally splitting the actual contents
  tmpColDelim = String.fromCharCode(11), // vertical tab character
  tmpRowDelim = String.fromCharCode(0), // null character

  // actual delimiter characters for CSV format
  //colDelim = '"\t"',
  rowDelim = '\r\n',

  // Grab text from table into CSV formatted string
  csv = $rows.map(function (i, row) {
      var $row = $(row), $cols = $row.find('td');

      return $cols.map(function (j, col) {
          var $col = $(col), text = $col.attr('value');

          return text//.replace(/"/g, '""'); // escape double quotes

      }).get().join(tmpColDelim);

  }).get().join(tmpRowDelim)
      .split(tmpRowDelim).join(rowDelim)
      .split(tmpColDelim).join(colDelim),



  // Data URI
  csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);


  if (window.navigator.msSaveBlob) { // IE 10+
      //alert('IE' + csv);
      window.navigator.msSaveOrOpenBlob(new Blob([csv], {type: "text/plain;charset=utf-8;"}), "csvname.csv")
  } 
  else {
      $(this).attr({ 'download': filename, 'href': csvData, 'target': '_blank' }); 
  }
}

$("#csvSave").on('click', function (event) {
   // picker.files[0].name.replace(".csv", ".xsv")
   if (hasRating) {
    exportTableToCSV.apply(this, [$('#csvTable'), picker.files[0].name], $("#sep").val());
   } else {
    exportTableToCSV.apply(this, [$('#csvTable'), picker.files[0].name.replace(".csv", "_rated.csv")], $("#sep").val());
   }
  // IF CSV, don't do event.preventDefault() or return false
  // We actually need this to be a typical hyperlink
  });

/*$("#xx").on('click', function (event) {
  exportTableToCSV.apply(this, [$('#csv'), 'export.csv']);
}*/

function generateCountGroupByTable(){
  const table = $('#csvTable');
  const rows = table.find('tr');
  const counts = {};
  var s = 0;
  // Loop through table rows
  for (let i = 1; i < rows.length; i++) {
    const name = rows[i].cells[0].getAttribute('value');
    console.log("%s: %s", i, name);
    // If name already exists in counts, increment count
    if (counts.hasOwnProperty(name)) {
      counts[name]++;
    } else { // Otherwise, add name to counts with count of 1
      counts[name] = 1;
    }
    s+=1;
  }

  // Create new table with grouped data
  let newTable = document.getElementById('csvSummary');
  newTable.innerHTML = '';
  //const newTable = document.createElement('table');
  let headerRow = newTable.insertRow(); //document.createElement('tr');
  headerRow.setAttribute('style', 'background-color:lightgray')
  let nameHeader = headerRow.insertCell();
  let countHeader = headerRow.insertCell();
  let percentageHeader = headerRow.insertCell();
  nameHeader.textContent = 'Rating';
  countHeader.textContent = 'Count';
  percentageHeader.textContent = '%';
  //headerRow.appendChild(nameHeader);
  //headerRow.appendChild(countHeader);
  //newTable.appendChild(headerRow);

  // Loop through counts object and create new rows in table
  for (let name in counts) {
    //const newRow = document.createElement('tr');
    //const nameCell = document.createElement('td');
    //const countCell = document.createElement('td');
    let newRow = newTable.insertRow(); 
    let nameCell = newRow.insertCell();
    let countCell = newRow.insertCell();
    let percentage = newRow.insertCell();
    nameCell.textContent = name;
    countCell.textContent = counts[name];
    percentage.textContent = Math.round(counts[name]/s*100, 1) + "%";
    //newRow.appendChild(nameCell);
    //newRow.appendChild(countCell);
    //newTable.appendChild(newRow);
  }

  let tailRow = newTable.insertRow();
  tailRow.setAttribute('style', 'background-color:lightgreen')
  let nameTail = tailRow.insertCell();
  let countTail = tailRow.insertCell();
  let percentageTail = tailRow.insertCell();
  nameTail.textContent = 'Sum';
  countTail.textContent = s;
  percentageTail.textContent = "100%";

  // Append new table to document
  //table.parentNode.appendChild(newTable);

}

// the button was removed, thus, the click event no longer needed
//$("#summaryUpdate").on('click', function(event){
//  generateCountGroupByTable(this);
//})
//picker = document.getElementById('csvPick')