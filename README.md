# csv-annotation-tool

## Purpose

It's a simple html+js tool to manually evaluate the rows from the csv file with a five-star rating (useful in machine learning).

## Looks

![Screenshot](./screenshot.jpg)

## How it works

One can load a csv file (manually choosing the separator).  
On load, a column with a five-star rating gets appended as the first column. 
While giving stars, a summary table with counts of the given stars (e.g. checking for 0s one can spot the un-evaluated rows) gets generated.  
After the manual evaluation it's possible to save the file.  
The five star rating is saved as column 'rating'.  
The file is saved by default as ${ORIGINAL_FILE_NAME}_rated.csv, i.e. the suffix '_rated' is added to the original filename.  

## TODOs

- if a file with a five-start column is loaded, then the five-start column is filled with the values from the file. 
- make html table sortable by columns.

## Used work

CSV parsing: [jquery-csv](https://github.com/evanplaice/jquery-csv)  
5 star rendering: [w3schools](https://www.w3schools.com/howto/howto_css_star_rating.asp)  
used as template: [code-boxx](https://code-boxx.com/display-csv-table-javascript/)  